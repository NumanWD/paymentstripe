<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
/**
 * @since 1.5.0
 */
use PrestaShop\PrestaShop\Adapter\Cart\CartPresenter;

class PaymentStripeValidationModuleFrontController extends ModuleFrontController
{

    /**
     * @see FrontController::postProcess()
     */
    public function postProcess()
    {

        \Stripe\Stripe::setApiKey(_STRIPE_SECRET_KEY);

        /** ---------------------   Check If cart Exist */
        $cart = $this->context->cart;
        if ($cart->id_customer == 0 ||
            $cart->id_address_delivery == 0 ||
            $cart->id_address_invoice == 0 ||
            !$this->module->active)
        {
            Tools::redirect('index.php?controller=order&step=1');
        }


        /** ---------------------   Check payment option */
        // Check that this payment option is still available in case the customer changed his address just before the end of the checkout process
        $authorized = false;
        foreach (Module::getPaymentModules() as $module) {
            if ($module['name'] == 'paymentstripe') {
                $authorized = true;
                break;
            }
        }
        if (!$authorized) {
            die($this->module->l('This payment method is not available.', 'validation'));
        }


        /** ---------------------   Check Validate Order */
        $customer = new Customer($cart->id_customer);
        if (!Validate::isLoadedObject($customer)) {
            Tools::redirect('index.php?controller=order&step=1');

        }


        /** ---------------------   Get Order details */
        $currency = $this->context->currency;
        $total = $cart->getOrderTotal(true, Cart::BOTH);
        $presenter = new CartPresenter;
        $presented_cart = $presenter->present($this->context->cart);


        /** ---------------------   Call Stripe */
        $stripe = $this->paymentStripe($total, $currency, $presented_cart, $customer);

        /** ---------------------   Process the Order */
         $this->module->validateOrder(
             $cart->id,
             Configuration::get('PS_OS_PAYMENT'),
             (float)$total,
             $this->module->displayName,
             'Pay with Stripe', ['transaction_id' => $stripe['id']],
             (int)$currency->id,
             false,
             $customer->secure_key
         );

        $this->postPaymentUpdate($stripe['id'], $this->module->currentOrder);

        /** ---------------------   Redirect the Order Confirmation */
        Tools::redirect('index.php?controller=order-confirmation&id_cart='.$cart->id.'&id_module='.$this->module->id.'&id_order='.$this->module->currentOrder.'&key='.$customer->secure_key);

    }

    private function paymentStripe($total, $currency, $cart, $customer)
    {

        $number = str_replace(' ', '', $_REQUEST['cc-number']);
        $cvc = $_REQUEST['cc-cvc'];
        $expire = [];
        $total = $total * 100;
        $myCard = [];
        $metadata = [];

        preg_match("/(\d+)\s*\/\s*(\d+)/", $_REQUEST['cc-exp'], $expire);

        $myCard =
            [
                'name' => "$customer->firstname $customer->lastname",
                'number' => $number,
                'exp_month' => $expire[1],
                'exp_year' => $expire[2],
                'cvc' => $cvc
            ];

        $metadata =
            [
                'customer_id' => $customer->id,
                'email' => $customer->email,

            ];

        try {
            $stripe = \Stripe\Charge::create(
                [
                    'card' => $myCard,
                    'amount' => $total,
                    'currency' => $currency->iso_code,
                    'description' => $cart['summary_string'],
                    "metadata" => $metadata
                ]
            );
            return $stripe;

        } catch(\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];

            $this->errors = 'Sorry :( - '.$err['message'];
            return $this->redirectWithNotifications('index.php?controller=order');

//        } catch (\Stripe\Error\RateLimit $e) {
//            echo ""
//            // Too many requests made to the API too quickly
//        } catch (\Stripe\Error\InvalidRequest $e) {
//            // Invalid parameters were supplied to Stripe's API
//        } catch (\Stripe\Error\Authentication $e) {
//            // Authentication with Stripe's API failed
//            // (maybe you changed API keys recently)
//        } catch (\Stripe\Error\ApiConnection $e) {
//            // Network communication with Stripe failed
//        } catch (\Stripe\Error\Base $e) {
//            // Display a very generic error to the user, and maybe send
            // yourself an email
        } catch (Exception $e) {
            $this->errors = 'Sorry :( - Look like There was a issue, Contact us.';
            return $this->redirectWithNotifications('index.php?controller=order');
        }
    }

    private function postPaymentUpdate($stripeId, $order)
    {

        $ch = \Stripe\Charge::retrieve($stripeId);
        $ch->metadata['order_id'] = $order;
        $ch->save();
    }
}
