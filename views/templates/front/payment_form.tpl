{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<form method="POST" action="{$action}" id="payment-stripe" autocomplete="on" novalidate>

  <div class="form-group">
      {*<input type="text"*}
             {*class="form-control"*}
             {*value=""*}
             {*name="card-number"*}
             {*placeholder="{l s='Card Number' mod='paymentstripe'}"*}
             {*required maxlength="16">*}
    <input id="cc-number"
           name="cc-number"
           type="tel"
           class="form-control cc-number"
           autocomplete="cc-number"
           placeholder="{l s='Card Number' mod='paymentstripe'}" required>

  </div>
  <div class="form-group">
      {*<input type="text"*}
             {*class="form-control"*}
             {*value=""*}
             {*name="card-cvc"*}
             {*placeholder="{l s='CVC' mod='paymentstripe'}"*}
             {*required maxlength="3">*}
     {*<span class="form-control-comment">*}
        {*{l s='3 digits on the back.'}*}
      {*</span>*}

      <input id="cc-cvc"
             name="cc-cvc"
             type="tel"
             class="form-control cc-cvc"
             autocomplete="off"
             placeholder="{l s='CVC' mod='paymentstripe'}" required>

  </div>
  <div class="form-group">

    <input id="cc-exp"
           name="cc-exp"
           type="tel"
           class="form-control cc-exp"
           autocomplete="cc-exp" placeholder="{l s='Expire Month / Year' mod='paymentstripe'}" required>

    {*<div class="form-inline">*}
      {*{l s='Expire' mod='paymentstripe'}*}
      {*<select id="month" name="card-expiry-month" class="form-control">*}
        {*{foreach from=$months item=month}*}
          {*<option value="{$month}">{$month}</option>*}
        {*{/foreach}*}
      {*</select>*}
      {*<span> / </span>*}
      {*<select id="year" name="card-expiry-year" class="form-control">*}
        {*{foreach from=$years item=year}*}
          {*<option value="{$year}">{$year}</option>*}
        {*{/foreach}*}
      {*</select>*}
    {*</div>*}
  </div>
</form>
